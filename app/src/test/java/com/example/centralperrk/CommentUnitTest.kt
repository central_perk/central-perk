package com.example.centralperrk

import com.example.centralperrk.Data.commentContent
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertFalse
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.w3c.dom.Comment

class CommentUnitTest(){
    lateinit var comment: commentContent
    @Before
    fun setUp() {
        comment = commentContent(1, "about", "name", "description")
    }
    @Test
    fun commentnotnull(){
        assertFalse(comment.name.isEmpty())

    }
    @Test
    fun commenttostring(){
        assertEquals("user1",comment.name)
    }
    @Test
    fun delecomment(){
        val samplecoment = commentContent(1, "about", "name", "description")
        val delete = samplecoment.toString().isEmpty()
        assertNotEquals(delete,samplecoment)

    }

}