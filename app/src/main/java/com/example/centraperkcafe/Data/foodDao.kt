package com.example.centralperk.Data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query


@Dao
interface foodDao {

    @Insert
    fun save (food: food):Long

    @Query("Select * FROM food WHERE type = :typeOne")
    fun getByType (typeOne: String):List<food>

    @Query("select * from food where type = :foodID")
    fun getById (foodID: Int): food

}