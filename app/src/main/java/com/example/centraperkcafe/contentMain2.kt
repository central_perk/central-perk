package com.example.centralperk


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.centraperkcafe.R
import kotlinx.android.synthetic.main.fragment_content_main2.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 *
 *
 * A simple [Fragment] subclass.
 *
 */
class contentMain2 : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_content_main2, container, false)
        val layoutManager = LinearLayoutManager(view.context)
        view.recycler_view.layoutManager = layoutManager


        val adapter = cafeAdapter(view.context, Supplier.lists)
        view.recycler_view.adapter = adapter

        return view
    }




}
