package com.example.centralperk

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import androidx.recyclerview.widget.RecyclerView
import com.example.centralperk.Data.food
import kotlinx.android.synthetic.main.recycler_view_item.view.*

class catagoryAdapter(val context: Context, val lists: List<list>): RecyclerView.Adapter<catagoryAdapter.catagoryViewHolder>(){


    var listOfFoods :List<food> = emptyList()

    fun setList(listOfFood:List<food>){
        this.listOfFoods= listOfFoods
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): catagoryViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.one_item_view,parent,false)
        return catagoryViewHolder(view)

    }

    override fun getItemCount(): Int {
        return lists.size
    }

    override fun onBindViewHolder(holder: catagoryViewHolder, position: Int) {
        val list =  lists[position]
        holder.setData(list, position)

    }


    inner class catagoryViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){

        fun setData(list: list?, pos : Int){
            itemView.cupCake_TV.text = list!!.title

        }

    }
}

