package com.example.centralperk

import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import com.example.centralperk.Data.cafe_data
import com.example.centralperk.Data.food
import com.example.centralperk.Repository.foodRepo
import com.example.centraperkcafe.R

import kotlinx.android.synthetic.main.recycler_view_item.view.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



       AsyncTask.execute{
              val dao = cafe_data.getDatabase(this).foodDao()
              val foodRepo= foodRepo(dao)
              foodRepo.save(food(1, 100, "cupCake", "Cup Cake"))
              foodRepo.save(food(2, 100, "Chips", "Fast Food"))
              foodRepo.save(food(3, 100, "Sambusa", "Cake"))
              foodRepo.save(food(4, 100, "Coca Cola", "Soda"))
        }
       // setSupportActionBar(toolbar)




    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
    fun to_detail(view: View){
        Toast.makeText(this,view.cupCake_TV.text.toString(),Toast.LENGTH_LONG).show()

        var bundle= bundleOf("catSelect" to view.cupCake_TV)

        val navController=findNavController(R.id.fragmentContainer)
        navController.navigate(R.id.action_contentMain2_to_catagoryDetail,bundle)
    }
}
