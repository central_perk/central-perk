package com.example.centralperk

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycler_view_item.view.*


class cafeAdapter(val context: Context, val lists: List<list>): RecyclerView.Adapter<cafeAdapter.cafeViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): cafeViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item,parent,false)
        return cafeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lists.size
    }

    override fun onBindViewHolder(holder: cafeViewHolder, position: Int) {
        val list =  lists[position]
        holder.setData(list, position)
    }

    inner class cafeViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){

        var currentList: list? = null
        var currentPosition: Int = 0

      //  init {
         //   itemView.setOnClickListener{
               // Toast.makeText(context, currentList!!.title+ " Clicked ! ", Toast.LENGTH_SHORT).show()
           // }
      //  }

        fun setData(list: list?, pos : Int){
            itemView.cupCake_TV.text = list!!.title
            this.currentList = list
            this.currentPosition = pos
        }
    }
}
