package com.example.centralperk

import android.content.Context

import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.centralperk.Data.cafe_data
import com.example.centralperk.Data.food
import com.example.centralperk.Repository.foodRepo

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_content_main2.*
import kotlinx.android.synthetic.main.recycler_view_item.*
import kotlinx.android.synthetic.main.recycler_view_item.view.*

const val TEXT_KEY = "textKey"
const val SHARED_PREFERENCE_ID = "shared_Pref"
const val IMAGE_KEY = "imageKey"

class MainActivity : AppCompatActivity() {

    lateinit var sharedPref :SharedPreferences
    lateinit var textView: TextView
    lateinit var imageView: ImageView

    lateinit var description_textView: TextView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val cafe_adapter = cafeAdapter(this)
        recycler_view.adapter = cafe_adapter
        recycler_view.layoutManager = LinearLayoutManager(this)

        sharedPref = getSharedPreferences(SHARED_PREFERENCE_ID, Context.MODE_PRIVATE)

        textView = cupCake_TV
        imageView = cupCake_IV





            AsyncTask.execute{

           // cafe_data = cafe_data.getDatabase(this)
              val dao = cafe_data.getDatabase(this).foodDao()
              val foodRepo= foodRepo(dao)
              foodRepo.save(food(1, 100, "cupCake", "Cup Cake"))
              foodRepo.save(food(2, 100, "Chips", "Fast Food"))
              foodRepo.save(food(3, 100, "Sambusa", "Cake"))
              foodRepo.save(food(4, 100, "Coca Cola", "Soda"))
        }
        //setSupportActionBar(toolbar)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
    fun to_detail(view: View){
        Toast.makeText(this,view.cupCake_TV.text.toString(),Toast.LENGTH_LONG).show()

        var bundle= bundleOf("catSelect" to view.cupCake_TV.text.toString())

       // val navController=findNavController(R.id.fragmentContainer)
       // navController.navigate(R.id.action_contentMain2_to_catagoryDetail,bundle)
    }
}
