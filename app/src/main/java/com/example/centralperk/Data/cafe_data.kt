package com.example.centralperk.Data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database (entities = arrayOf(food:: class),version = 1)

abstract class cafe_data : RoomDatabase(){
    abstract fun foodDao():foodDao
    companion object {
        @Volatile
        private var INSTANCE :cafe_data?=null

        fun getDatabase(context: Context):cafe_data{
            val tempInstance= INSTANCE
            if(tempInstance!=null){
                return tempInstance
            }
            synchronized(this){
                val instance= Room.inMemoryDatabaseBuilder(
                    context.applicationContext,
                    cafe_data::class.java)
                    .fallbackToDestructiveMigration()
                    .build()
                return instance
            }
        }
    }



}