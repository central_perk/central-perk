package com.example.centralperk.Data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface foodDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFood(food: food):Long
    fun save (food: food):Long

    @Query("Select * FROM food WHERE type = :typeOne")
    fun getByType (typeOne: String):List<food>
    fun getAllItem():LiveData<List<food>>

    @Query("select * from food where type = :foodID")
    fun getById (foodID: Int): food

}