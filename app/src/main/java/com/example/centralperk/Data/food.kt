package com.example.centralperk.Data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName = "cafe_data")
class food (@PrimaryKey @ColumnInfo(name = "fID") val fID:Int,
            @ColumnInfo(name = "amount") val amount:Int,
            @ColumnInfo(name = "image_name") val image_name: String,
            @ColumnInfo(name = "image_type") val image_type: String):Serializable


