package com.example.centralperk.Repository

import androidx.lifecycle.LiveData
import com.example.centralperk.Data.cafe_data
import com.example.centralperk.Data.foodDao
import com.example.centralperk.Data.food

class foodRepo (val foodDao: foodDao){

    fun menuLsts():LiveData<List<food>> = foodDao.getById()

    fun insertFood(food: food){
        foodDao.insertFood(food)
    }


    fun save (food: food):Long{

        return foodDao.save(food)

    }

    fun getByType (typeOne: String):List<food>{
        return foodDao.getByType(typeOne)

    }

    fun getById (foodID: Int): food {
        return foodDao.getById(foodID)

    }
}