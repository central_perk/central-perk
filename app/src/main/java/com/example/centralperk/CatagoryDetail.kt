package com.example.centralperk


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_catagory_detail.view.*


// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class CatagoryDetail : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_catagory_detail, container, false)
        val layoutManager = LinearLayoutManager(view.context)
        view.catagoryRV.layoutManager = layoutManager

        val cat = arguments?.getString("catSelect")
        val stringCat = if (cat!=null) cat.toString() else ""


        val adapter = cafeAdapter(view.context, Supplier.lists)
        view.catagoryRV.adapter = adapter

        return view
    }


}
