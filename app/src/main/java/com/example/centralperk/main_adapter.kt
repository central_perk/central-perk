package com.example.centralperk

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.centralperk.Data.food
import kotlinx.android.synthetic.main.main_activity_recycler_view.view.*

class main_adapter(val context: Context, val lists: List<list>): RecyclerView.Adapter<main_adapter.catagoryViewHolder>(){
    override fun getItemCount(): Int {
        return lists.size
    }

    override fun onBindViewHolder(holder: catagoryViewHolder, position: Int) {
        val list =  lists[position]
        holder.setData(list, position)
    }
    var listOfFoods :List<food> = emptyList()

    fun setList(listOfFood:List<food>){
        this.listOfFoods= listOfFoods
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): catagoryViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.main_activity_recycler_view,parent,false)
        return catagoryViewHolder(view)

    }
    inner class catagoryViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){

        var currentList: list? = null
        var currentPosition: Int = 0

        fun setData(list: list?, pos : Int){
            itemView.cupCake_TV.text = list!!.title
            this.currentList = list
            this.currentPosition = pos
        }
}}