package com.example.centralperk

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_fragment_list_of_item.view.*
import kotlinx.android.synthetic.main.main_activity_recycler_view.view.*
import kotlinx.android.synthetic.main.recycler_view_item.view.*
import kotlinx.android.synthetic.main.recycler_view_item.view.cupCake_TV
import kotlin.text.Typography.cent


class cafeAdapter(val context: Context, val lists: List<list>): RecyclerView.Adapter<cafeAdapter.cafeViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): cafeViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item,parent,false)
        return cafeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lists.size
    }

    override fun onBindViewHolder(holder: cafeViewHolder, position: Int) {
        val list =  lists[position]
        holder.setData(list, position)


        holder.textView.text = list.title


    }

    inner class cafeViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){

        val imageView = itemView.image_view
        val textView = itemView.textView

        var currentList: list? = null
        var currentPosition: Int = 0

        init {
           itemView.setOnClickListener{
               Toast.makeText(context, currentList!!.title+ " Clicked ! ", Toast.LENGTH_SHORT).show()
            }
        }

        fun setData(list: list?, pos : Int){

        //   var centeralperk = centeralperk[position]
        //   itemView.image_view.setImageResource(centeralperk.image)

            itemView.cupCake_TV.text = list!!.title
            this.currentList = list
            this.currentPosition = pos

            itemView.cupCake_IV.setImageResource(this.currentPosition)
        }
    }
}
