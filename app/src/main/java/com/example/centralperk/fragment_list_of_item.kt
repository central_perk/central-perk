package com.example.centralperk

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_catagory_detail.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [fragment_list_of_item.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [fragment_list_of_item.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class fragment_list_of_item : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_catagory_detail, container, false)
        val layoutManager = LinearLayoutManager(view.context)
        view.catagoryRV.layoutManager = layoutManager

        val cat = arguments?.getString("catSelect")
        val stringCat = if (cat!=null) cat.toString() else ""


        val adapter = main_adapter(view.context, Supplier.lists)
        view.catagoryRV.adapter = adapter

        return view
    }
    }

