package com.example.centralperk

data class list(var title: String)

object Supplier{

    val lists = listOf<list>(
        list("Cup Cake"),
        list("Soda"),
        list("Cake"),
        list("Fast Food"),
        list("Juice")
    )
}