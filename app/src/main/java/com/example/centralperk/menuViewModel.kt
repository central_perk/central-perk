package com.example.centralperk

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.centralperk.Data.cafe_data
import com.example.centralperk.Data.food
import com.example.centralperk.Repository.foodRepo
import okhttp3.Dispatcher

class menuViewModel (application: Application):AndroidViewModel(application){
    private val foodRepository: foodRepo
    val menuList: LiveData<List<food>>

    init {
        val foodDao = cafe_data.getDatabase(application).foodDao()
        foodRepository = foodRepo(foodDao)
        menuList = foodRepository.menuLsts()
    }

    fun insertFood(food: food)= menuViewModel.launch(Dispatcher.IO){
        foodRepository.insertFood(food)
    }
}