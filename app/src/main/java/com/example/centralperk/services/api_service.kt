package com.example.centralperk.services

import retrofit2.Call
import retrofit2.http.*

interface api_service {
    @GET("menu_list/{id}")
    fun getItemList(@Path("id")id: Long):Call<List<String>>
    @GET("posts")
    fun getPostsByUserId(@Query("imageId")name:Long):Call<List<String>>
    @PUT("post")
    fun updatePost(@Path("id")id:Long,@Body post: POST):Call<List<String>>
}