package com.example.centralperk.services

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object serviceBuilder {

    private const val URL = "http//10.0.2.2:9000"

    private val okHttp : OkHttpClient.Builder = OkHttpClient.Builder()

    private val builder: Retrofit.Builder =Retrofit.Builder().baseUrl(URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttp.build())

    private val retrofit:Retrofit  = builder.build()
    fun<T> buildService(serviceType:Class<T>): T {
        return retrofit.create(serviceType)
    }
}

