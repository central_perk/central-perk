package com.example.centralperrk

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView.layoutManager = LinearLayoutManager(this)

        //val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.
        //    layout.activity_main)

      /*  val user = menuDataBind("Kuma", "image")
        binding.setVariable(BR. user, user)
        binding.executePendingBindings()*/

        fetchJson()
    }

    private fun fetchJson(){
        println("Attempting to fetch Json")
    // enter the url in here
        val url = "http"

        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object : Callback{

            override fun onResponse(call: Call,response: Response) {
                val body = response.body()?.string()
                println(body)

                val gson = GsonBuilder().create()

                val homefeed = gson.fromJson(body,HomeFeed::class.java)


                runOnUiThread {
                    recyclerView.adapter = MenuAdapter(homefeed)
                }

            }
            override fun onFailure(call: Call,e: IOException) {
                println("Failed to execute Request")

            }
        })
    }

}

