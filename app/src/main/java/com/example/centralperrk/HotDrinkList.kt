package com.example.centralperrk

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class HotDrinkList : AppCompatActivity(){



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = HotDrinkListAdapter()

    }
    private class HotDrinkListAdapter: RecyclerView.Adapter<HotDrinkListAdapter.HotDrinkListViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HotDrinkListViewHolder {

           val layoutInflater = LayoutInflater.from(parent?.context)
            val customView = layoutInflater.inflate(R.layout.fragment_hot_drink_list,parent, false)
            return HotDrinkListViewHolder(customView)
        }

        override fun getItemCount(): Int {
            return 5
        }

        override fun onBindViewHolder(holder: HotDrinkListViewHolder, position: Int) {

        }
        private class HotDrinkListViewHolder(val view: View):RecyclerView.ViewHolder(view){

        }
    }
}