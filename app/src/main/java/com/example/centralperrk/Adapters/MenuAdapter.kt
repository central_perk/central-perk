package com.example.centralperrk.Adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.centralperrk.AllActivities.HotDrinkList
import com.example.centralperrk.ViewModel.HomeFeed
import com.example.centralperrk.R
import kotlinx.android.synthetic.main.fragment_hot_drink_list.view.*
import kotlinx.android.synthetic.main.fragment_recycler_view.view.*
import kotlinx.android.synthetic.main.recycler_view_item.view.*


class MenuAdapter(val homeFeed: HomeFeed): RecyclerView.Adapter<MenuAdapter.MenuAdapterViewHolder>() {

   val menuList  = listOf("Cup Ckae"
       ,"Juice","Hote Drink","Cakes", "Fast Food")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuAdapterViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.fragment_recycler_view, parent, false)
        return MenuAdapterViewHolder(cellForRow)

    }

    override fun getItemCount(): Int {
        return homeFeed.menu_list.count()
    }

    override fun onBindViewHolder(holder: MenuAdapterViewHolder, position: Int) {
        val menuLists = menuList.get(position)
        val menu = homeFeed.menu_list.get(position)

        holder.itemView.FastfoodTV?.text = menu.name
        holder.itemView.discription?.text = menu.description

        val fullPicImageView = holder.itemView.fullPic
        // picasso.with(holder?.itemView?.context).load(menu.imageUrl).into(fullPicImageView)
        val logoImageView = holder.itemView.logo
      //  picasso.with(holder?.itemView?.context).load(menu.logo)into(logoImageView)
    }

    inner class MenuAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, HotDrinkList::class.java)
                intent.putExtra("thisname", "MenuLIST")
                itemView.context.startActivity(intent)
            }
        }

        }

    }

