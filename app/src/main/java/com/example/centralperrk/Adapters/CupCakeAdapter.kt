package com.example.centralperrk.Adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.centralperrk.R
import kotlinx.android.synthetic.main.activity_main.*

private class CupCakeListAdapter: RecyclerView.Adapter<CupCakeListAdapter.CupCakeListViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CupCakeListViewHolder {

        val layoutInflater = LayoutInflater.from(parent?.context)
        val customView = layoutInflater.inflate(R.layout.fragment_cup_cake_list,parent, false)
        return CupCakeListViewHolder(customView)
    }

    override fun getItemCount(): Int {
        return 5

    }

    override fun onBindViewHolder(holder: CupCakeListViewHolder, position: Int) {

    }


    private class CupCakeListViewHolder(val view: View): RecyclerView.ViewHolder(view){

    }
}