package com.example.centralperrk

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_hot_drink_list.view.*
import kotlinx.android.synthetic.main.recycler_view_item.view.*


class MenuAdapter(val homeFeed: HomeFeed): RecyclerView.Adapter<MenuAdapter.MenuAdapterViewHolder>() {

   val menuList  = listOf("Cup Ckae"
       ,"Juice","Hote Drink","Cakes", "Fast Food")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuAdapterViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        val cellForRow = layoutInflater.inflate(R.layout.recycler_view_item, parent, false)
        return MenuAdapterViewHolder(cellForRow)

    }

    override fun getItemCount(): Int {
        return homeFeed.menu_list.count()
    }

    override fun onBindViewHolder(holder: MenuAdapterViewHolder, position: Int) {
        val menuLists = menuList.get(position)
        val menu = homeFeed.menu_list.get(position)

        holder.itemView.FastfoodTV?.text = menu.name
        holder.itemView.discription?.text = menu.description

        val fullPicImageView = holder.itemView.fullPic
        // picasso.with(holder?.itemView?.context).load(menu.imageUrl).into(fullPicImageView)
        val logoImageView = holder.itemView.logo
      //  picasso.with(holder?.itemView?.context).load(menu.logo)into(logoImageView)
    }

    inner class MenuAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, HotDrinkList::class.java)
                itemView.context.startActivity(intent)
            }
        }

        }

    }

