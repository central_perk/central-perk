package com.example.centralperrk

import android.os.AsyncTask
import android.os.AsyncTask.execute
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.centralperrk.Data.commentDao
import com.example.centralperrk.Data.commentDatabase
import kotlinx.android.synthetic.main.comment.*
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.NonCancellable.isActive

class comment : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.comment)

        addBtn = add
        updatebtn = update
        deletbtn = delet

        branchET = branch
        aboutET = about
        describtionET = des
        nameET = name


        add.setOnClickListener {  }
        update.setOnClickListener {  }
        delet.setOnClickListener {  }

       AsyncTask.execute{
            CommentDatabse = commentDatabase.getDatabase(this)
            CommentDao = CommentDatabse.commentDao

        }
    }

    private  var  CommentDatabse = commentDatabase
  //  private  var CommentDao = commentDao



    private lateinit var addBtn: Button
    private lateinit var updatebtn: Button
    private lateinit var deletbtn: Button

    private lateinit var branchET: EditText
    private lateinit var aboutET: EditText
    private lateinit var describtionET: EditText
    private lateinit var nameET: EditText

    @InternalCoroutinesApi
    internal fun getActiveAndCompletedStats(tasks): StatsResult {

        return if (tasks == null || tasks.isEmpty()) {
            StatsResult(0f, 0f)
        } else {
            val totalTasks = tasks.size
            val numberOfActiveTasks = tasks.count { .isActive }
            StatsResult(
                activeTaskPercent =  100f * numberOfActiveTasks / tasks.size,
                completedTaskPercent = 100f * (totalTasks - numberOfActiveTasks) / tasks.size
            )
        }
    }

   data class StatsResult(val activeTaskPercent: Float, val completedTaskPercent: Float) {

    }

}