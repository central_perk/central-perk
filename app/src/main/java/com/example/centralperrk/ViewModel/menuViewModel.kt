package com.example.centralperk

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.centralperrk.Data.commentContent
import com.example.centralperrk.Data.commentDatabase
import com.example.centralperrk.Repository.commentRepository

class menuViewModel (application: Application):AndroidViewModel(application){
    private val foodRepository: commentRepository
    val comment: LiveData<List<commentContent>>

    init {
        val commentDAO = commentDatabase.getDatabase(application).commentDao()
        foodRepository = commentRepository(commentDAO)
        comment = foodRepository.allcomment()
    }


}