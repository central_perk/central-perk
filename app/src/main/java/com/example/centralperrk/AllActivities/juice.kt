package com.example.centralperrk.AllActivities

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.centralperrk.R
import kotlinx.android.synthetic.main.activity_main.*

class juice : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = juiceAdapter()

    }
    private class juiceAdapter: RecyclerView.Adapter<juiceAdapter.juiceAdapterViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): juiceAdapterViewHolder {

            val layoutInflater = LayoutInflater.from(parent?.context)
            val customView = layoutInflater.inflate(R.layout.fragment_juice,parent, false)
            return juiceAdapterViewHolder(
                customView
            )
        }

        override fun getItemCount(): Int {
            return 5
        }

        override fun onBindViewHolder(holder: juiceAdapterViewHolder, position: Int) {

        }
        private class juiceAdapterViewHolder(val view: View): RecyclerView.ViewHolder(view){

            init {
                itemView.setOnClickListener {

                    val dialogBuilder = AlertDialog.Builder(it.context)

                    dialogBuilder.setMessage("should fetch from api or something")
                        .setCancelable(false)
                        .setPositiveButton("Order", DialogInterface.OnClickListener{

                            // val intent = Intent(itemView.context, HotDrinkList::class.java)
                            //  intent.putExtra("this name", "MenuLIST")

                                dialog, id ->
                        })
                        .setNegativeButton("Change", DialogInterface.OnClickListener {
                                dialog, which -> dialog.cancel() })

                    val alert = dialogBuilder.create()
                    alert.setTitle("Order")
                    alert.show()
                    /*  val intent = Intent(itemView.context, HotDrinkList::class.java)

                      intent.putExtra("this name", "MenuLIST")

                      itemView.context.startActivity(intent)*/


                }
            }

        }
    }
}