package com.example.centralperrk.AllActivities

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.centralperrk.R
import kotlinx.android.synthetic.main.activity_main.*

class fast_fod : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.fragment_fast_fod)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = fast_fodAdapter()

    }
    private class fast_fodAdapter: RecyclerView.Adapter<fast_fodAdapter.fast_fodAdapterViewHolde>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): fast_fodAdapterViewHolde {

            val layoutInflater = LayoutInflater.from(parent?.context)
            val customView = layoutInflater.inflate(R.layout.fragment_fast_fod,parent, false)
            return fast_fodAdapterViewHolde(
                customView
            )
        }

        override fun getItemCount(): Int {
            return 5
        }

        override fun onBindViewHolder(holder: fast_fodAdapterViewHolde, position: Int) {

        }
        private class fast_fodAdapterViewHolde(val view: View): RecyclerView.ViewHolder(view){

            init {
                itemView.setOnClickListener {

                    val dialogBuilder = AlertDialog.Builder(it.context)

                    dialogBuilder.setMessage("should fetch from api or something")
                        .setCancelable(false)
                        .setPositiveButton("Order", DialogInterface.OnClickListener{

                            // val intent = Intent(itemView.context, HotDrinkList::class.java)
                            //  intent.putExtra("this name", "MenuLIST")

                                dialog, id ->
                        })
                        .setNegativeButton("Change", DialogInterface.OnClickListener {
                                dialog, which -> dialog.cancel() })

                    val alert = dialogBuilder.create()
                    alert.setTitle("Order")
                    alert.show()
                    /*  val intent = Intent(itemView.context, HotDrinkList::class.java)

                      intent.putExtra("this name", "MenuLIST")

                      itemView.context.startActivity(intent)*/


                }
            }

        }
    }
}