package com.example.centralperrk.serviceApi

import com.example.centralperrk.Data.commentContent
//import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.example.centralperrk.comment
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

interface CommentServiceApi  {


    @GET("/meeting")
    fun getAllMeetings(): Deferred<Response<List<commentContent>>>



    companion object {
        private val baseUrl = ""
        fun getInstance(): CommentServiceApi{
            val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(MoshiConverterFactory.create())
               // .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
            return retrofit.create(CommentServiceApi::class.java)


        }
    }

}