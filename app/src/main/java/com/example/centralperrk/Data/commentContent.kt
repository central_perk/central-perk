package com.example.centralperrk.Data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName = "Comment")
data class commentContent(
    @PrimaryKey @ColumnInfo (name = "which_branch")val branch: Int,
    @ColumnInfo (name = "What_is_it_about") val about:String,
    @ColumnInfo(name="customer_name")val name:String,
    @ColumnInfo(name = "description")val description: String):Serializable

