package com.example.centralperrk.Data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.centralperrk.comment

@Dao

interface commentDao {
    @Query("SELECT * FROM comment ORDER BY which_branch ")
    fun getAllComment():LiveData<List<commentContent>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCommentContent(comment: commentContent):Long

    @Update
    fun updateCommentContent(comment: commentContent):Int

    @Delete
    fun deleteCommentContent(comment: commentContent):Int


}
