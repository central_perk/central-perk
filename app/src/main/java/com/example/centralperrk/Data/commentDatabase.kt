package com.example.centralperrk.Data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(commentContent::class), version = 2)

abstract class commentDatabase:RoomDatabase() {
    abstract fun commentDao(): commentDao

    companion object{
        @Volatile
        private var INSTANCE: commentDatabase? = null

        fun getDatabase(context: Context): commentDatabase {
            val tempInstance = INSTANCE
            if(tempInstance !=null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    commentDatabase::class.java, "comment_database"
                ).build()

                INSTANCE = instance
                return instance
            }
        }
    }
}
