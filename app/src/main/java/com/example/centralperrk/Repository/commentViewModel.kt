package com.example.centralperrk.Repository

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.centralperrk.Data.commentContent
import com.example.centralperrk.Data.commentDatabase
import com.example.centralperrk.serviceApi.CommentServiceApi
import okhttp3.Dispatcher
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

class commentViewModel(application: Application): AndroidViewModel(application) {

    private val commentRepo : commentRepository
    val allComment: LiveData<List<commentContent>>

    init {
        val commentDao  = commentDatabase.getDatabase(application).commentDao()
        commentRepo = commentRepository(commentDao, CommentServiceApi.getInstance())
        allComment = commentRepo.allcomment()
    }
    fun insertComment(comment: commentContent) =viewModelScope.launch(Dispatcher.IO){
        commentRepo.insertComment(comment)
    }
}