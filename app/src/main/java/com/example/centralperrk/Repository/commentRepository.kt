package com.example.centralperrk.Repository

import androidx.lifecycle.LiveData
import com.example.centralperrk.Data.commentContent
import com.example.centralperrk.Data.commentDao
import com.example.centralperrk.serviceApi.CommentServiceApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class commentRepository(private val commentDao: commentDao, private val commentServiceApi: CommentServiceApi) {
    fun allcomment():LiveData<List<commentContent>> = commentDao.getAllComment()

    fun insertComment(comment: commentContent){
        commentDao.insertCommentContent(comment)
    }
    suspend fun getAllMeetings(): Response<List<commentContent>> =
    withContext(Dispatchers.IO){
        commentServiceApi.getAllMeetings().await()
    }
}