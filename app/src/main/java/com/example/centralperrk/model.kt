package com.example.centralperrk

data class Menu(var name: String) {

}

object Supplier {
    var menus = listOf(
        Menu("Cup Cake"),
        Menu("Hot Drinks"),
        Menu("Soft Drinks"),
        Menu("Fast Food"),
        Menu("Cake"),
        Menu("Juice")
    )
}
class HomeFeed(val menu_list: List<Menus>)
class Menus(val id: Int,
            val imageUrl: String,
            val logo: String,
            val name: String,
            val description: String,
            val amount: Int)