package com.example.centralperrk.data

import android.app.Application
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.example.centralperrk.Data.commentContent
import com.example.centralperrk.Data.commentDao
import com.example.centralperrk.Data.commentDatabase
import com.example.centralperrk.Repository.commentRepository
import com.example.centralperrk.Repository.commentViewModel
import com.example.centralperrk.serviceApi.CommentServiceApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import org.junit.*

class CommentViewModelTest {
    private lateinit var appDatabase: commentDatabase
    private lateinit var viewModel: commentViewModel
    private lateinit var commentDao: commentDao
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room.inMemoryDatabaseBuilder(context, commentDatabase::class.java).build()
        commentDao = appDatabase.commentDao()

        commentDao = appDatabase.commentDao()

        val chatRepo = commentRepository(commentDao, CommentServiceApi.getInstance())
        viewModel = commentViewModel(Application())
    }
    @After
    fun tearDown() {
        appDatabase.close()
    }

    @Test
    fun sendMessage(){
        lateinit var result : Job
        runBlocking {
            result = viewModel.insertComment(commentContent(1, "about", "name", "description"))
        }
        Assert.assertNotEquals(null,result)
    }
}