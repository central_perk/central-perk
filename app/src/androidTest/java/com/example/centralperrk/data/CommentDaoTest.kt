package com.android.airmart.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.platform.app.InstrumentationRegistry
import com.android.airmart.data.dao.CommentDao
import com.android.airmart.data.entity.Comment
import com.android.airmart.utilities.testComments
import com.example.centralperrk.Data.commentDao
import com.example.centralperrk.Data.commentDatabase
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.w3c.dom.Comment


class CommentDaoTest{
    private lateinit var database:commentDatabase
    private lateinit var commentDao: commentDao
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before fun createDatabase(){
        val context  = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context,commentDatabase::class.java).build()
        commentDao = database.commentDao()
        testInsertComment().forEach{comment->
            database.commentDao().insertCommentContent(comment)
        }
    }
    @After fun closeDatabase(){
        database.close()
    }

    @Test fun testGetAllCommentsByProductId(){
        assertThat(commentDao.getAllComment().value?.size,equalTo(1))
    }
    @Test fun testInsertComment(){
        var insertCommentContent = commentDao.insertCommentContent(Comment(1, 1, "description", "username"))
        assertThat(commentDao.getAllComment().value?.size, equalTo(2))
    }
    @Test fun testDeleteComment(){
        var deleteCommentContent = commentDao.deleteCommentContent(testInsertComment()[])
        assertThat(commentDao.getAllComment(1).value?.size, equalTo(0))
    }

}